### Small IoT - ML Bridge for the SAP Hackathon

This script will spawn an http server that allows to query for the current solution for the TSP problem
as well as the queue times

### Usage

- Install the packages: `yarn install`
- Start the server using: `node server.js`
- The process will take quite a while to finish modify the start() method (See in Code)


