const https = require('https'),
    rp = require('request-promise'),
    fs = require('fs'),
    moment = require('moment'),
    express = require("express");
const app = express();

const TOKEN_URL = "https://ml-cebit-01.authentication.eu10.hana.ondemand.com/oauth/token"
const TIMESERIES_URL ="https://ml-cebit-05-timeseries.cfapps.eu10.hana.ondemand.com/forecastTrigger"
const TIMESERIES_RESULT_URL = "https://ml-cebit-05-timeseries.cfapps.eu10.hana.ondemand.com/forecastGetResult?jobId="
const TSP_URL = "https://ml-cebit-05-tsp.cfapps.eu10.hana.ondemand.com/find"
const PARAMS = "?grant_type=client_credentials&response_type=token&client_id=sb-197be07e-e543-400a-8779-26d5f96da607!b3675%7Ciotae_service!b5&client_secret=vcpjXMAV5prl0FeRJNyQ2RBdQSA%3D"

var queue_lengths = []
var route = []
const attractions = [
    "FA0D6680E7CD4B1C8700D2DD9730DBF5",
    "A0980BD3190C4D259719BC4FDED35C74",
    "0563502A95804856BA661BDDAB876D87",
    "0ABDB562FE18406DB08E185E7D687408",
    "DD2471461675459EBC4F2BD0AB0FC428",
    "2B186EBA57A04574A41C5EC1C2006664",
    "6D3825F5D39D4B679F3A493BC39722C8",
    "E2E95D31A6D540A3AE1E4C508E2934D1",
    "866D9E6077B04737B2C754C8EA509AA2",
    "EDAFFDAE3A234F6E9A16CA5729D2FF64",
    "F2173E1EBBD8451DBEC2EEA379FCF10D",
    "6CCB02E8D9834A32B7023C5142C88036",
    "B889888AD74F4873BFA49B06EFD70861",
    "4DF3891EDCF7446D9139116BDA1E9A11",
    "B296DF70060048E8898E5ECA2729C438",
    "AE5E3DA74EC0448CBD0CDEBFA574D3BF",
    "3324FA2392C24EE4B690D1C283C3502A",
    "18D643447D00433F82643E4A35506EC5",
    "3F465C872ABF428383478DB3CFAEDF97"
]
async function login() {
    try{
        var option = {
            method: 'POST',
            uri: TOKEN_URL + PARAMS,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/x-www-form-urlencoded'
            },
            json: true
        };
        const req = await rp(option)
        return req.access_token
    } catch(err){
        console.log(err)
    }
}

async function getInfo(attractionID){
    try{
    const token = await login()
    console.log("Getting AtractionInfo #" + attractionID)
    var option = {
        method: 'GET',
        uri: 'https://appiot-mds.cfapps.eu10.hana.ondemand.com/Things%28%27'+ attractions[attractionID] + '%27%29/ml.cebit01.amusement.park:Attractions/AttractionInfo',
        headers: {
            "authorization": "Bearer " + token,
            'cache-control': 'no-cache',
        },
        json: true
    };
    const req = await rp(option)
    console.log(req)
    return req
    } catch (e) {
        console.log(e)
    }
}
async function getData(timerange) {
    try {
        const token = await login()
        var arr = [];
        for (i = 0; i < attractions.length; i++){
            console.log("Getting Atraction #" + i)
            var option = {
                method: 'GET',
                uri: 'https://appiot-mds.cfapps.eu10.hana.ondemand.com/Things%28%27'+ attractions[i] + '%27%29/ml.cebit01.amusement.park:Attractions/QueueLength?timerange='+timerange,
                headers: {
                    "authorization": "Bearer " + token,
                    'cache-control': 'no-cache',
                },
                json: true
            };
            const req = await rp(option)
            arr.push(req.value)
        }
        //61e527bc-4130-4b99-ae65-ca7d2c6bba8c
        var res = "datetime, a1-queue-size, a2-queue-size, a3-queue-size, a4-queue-size, a5-queue-size, a6-queue-size, a7-queue-size, a8-queue-size, a9-queue-size, a10-queue-size, a11-queue-size, a12-queue-size, a13-queue-size, a14-queue-size, a15-queue-size, a16-queue-size, a17-queue-size, a18-queue-size, a19-queue-size\n"
        for(i = arr[0].length - 1; i >= 0; i--){
            var line = [moment(arr[0][i]._time).format("YYYY-MM-DD HH:mm:SS")]
            for(j = 0; j < attractions.length; j++){
                if(arr[j][i] != undefined){
                    line.push(arr[j][i].QueueLength)
                } else {
                    line.push(0)
                }
            }
            res += line.join(",") + "\n"
        }
        fs.writeFile("./values.csv", res, function(err) {
            if(err) {
                return console.log(err);
            }
        });
    }
    catch(e) {
        console.error(`Something went wrong: ${e.message}`);
    }
}

async function postSeries(){
    try {
        var options = {
            method: 'POST',
            uri: TIMESERIES_URL,
            formData: {
                attractionsNumber: 19,
                sensorsMinuteStep: 1,
                file: {
                    value: fs.createReadStream('./values.csv'),
                    options: {
                        filename: 'values.csv'
                    }
                }
            }
        };
        const jobId = await rp(options)
        return jobId
    } catch (e) {
        console.log(e)
    }
}

async function getResult(jobId){
    try {
        var options = {
            method: 'GET',
            uri: TIMESERIES_RESULT_URL + jobId,
        };
        const results = await rp(options)
        return results
    } catch (e) {
        console.log(e)
    }
}

async function getTSPRoute(){
    try {
        var options = {
            method: 'POST',
            uri: TSP_URL,
            formData: {
                startState: 0,
                endState: 20,
                attractionsNumber: 19,
                file_walking_rewards: {
                    value: fs.createReadStream('./walking_rewards.csv'),
                    options: {
                        filename: 'walking_rewards.csv'
                    }
                },
                file_waiting_rewards: {
                    value: JSON.stringify(queue_lengths),
                    options: {
                        filename: 'waiting_rewards.json'
                    }
                }
            }
        };
        const route = await rp(options)
        return route
    } catch (e) {
        console.log(e)
    }
}

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function waitForResult(jobId){
    try{
        const res = await getResult(jobId)
        if(res == "Job Pending"){
            console.log("Resending next Message in 30s")
            await timeout(30000);
            await waitForResult(jobId)
        } else {
            return JSON.parse(res)
        }
    } catch (e) {
        console.log(e)
    }
}

async function start(){
    try{
        /*
         * Uncomment this to do a training / recalculation
         */
        //console.log("Getting Data from sensors")
        //await getData("6M")
        //console.log("Starting training")
        //const jobId = await postSeries()
        const jobId = "99107324-2c4c-4d58-8933-a2d9287880c5" // Pretrained jobId
        console.log("Waiting for Results for: " + jobId)
        queue_lengths = await waitForResult(jobId)
        console.log("Done")
    } catch(e) {
        console.log(e)
    }
}

start()

app.get("/attractions", function(req, res) {
   res.setHeader('Content-Type', 'application/json');
   res.send(queue_lengths);
});

app.get("/queuelength", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    attraction_avgs = []
    for(var i = 0; i < queue_lengths.length; i++){
        attraction_avg = []
        for(var hour = 0; hour < 8; hour++){
            var avg = 0
            for(var minute = 0; minute < 60; minute++){
                avg += queue_lengths[i][(hour*60) + minute]
            }
            attraction_avg.push(avg / 60)
        }
        attraction_avgs.push(attraction_avg)
    }
    res.send(attraction_avgs);
 });

app.get("/route", async function(req,res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(await getTSPRoute());
})

app.listen(5000, function() {
  console.log("Listening on " + 5000);
});